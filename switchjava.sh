#!/bin/sh

echo "Switching to java -v 1.$1"
VER="/usr/libexec/java_home -v 1.$1"
JHOME=$(${VER})
echo $JHOME
export JAVA_HOME=$JHOME
echo "JAVA_HOME is now : $JAVA_HOME"
java -version
