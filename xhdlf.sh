#!/bin/bash
if [ -f $1 ] && [ -s $1 ]; then
    if [ -r $1 ]; then
		while read line
		do
			./xhdl.sh "$line"
		done <$1
    else
    	echo "File is not readable"
    	exit 1
    fi
fi
