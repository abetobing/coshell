#!/bin/bash
#sudo ipfw flush
# ipfw is no longer supported since Yosemite (Mac 10.10)
# Now using pfctl
sudo pfctl -d
# Flush all rules
sudo pfctl -F all -f /etc/pf.conf
