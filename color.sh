#!/bin/bash
# Description of script
 
# Required program(s)
# req_progs=(prog1 prog2)
# for p in ${req_progs[@]}; do
#   hash "$p" 2>&- || \
#   { echo >&2 " Required program \"$p\" not installed."; exit 1; }
# done
 
Display usage if no parameters given
if [[ -z "$@" ]]; then
  echo " ${0##*/} <input> - description"
  exit
fi
 
# Text color variables
txtred='\033[0;31m'       # red
txtgrn='\033[0;32m'       # green
txtylw='\033[0;33m'       # yellow
txtblu='\033[0;34m'       # blue
txtpur='\033[0;35m'       # purple
txtcyn='\033[0;36m'       # cyan
txtwht='\033[0;37m'       # white
bldred='\033[1;31m'       # red    - Bold
bldgrn='\033[1;32m'       # green
bldylw='\033[1;33m'       # yellow
bldblu='\033[1;34m'       # blue
bldpur='\033[1;35m'       # purple
bldcyn='\033[1;36m'       # cyan
bldwht='\033[1;37m'       # white
txtund=$(tput sgr 0 1)  # Underline
txtbld=$(tput bold)     # Bold
txtrst='\033[0m'          # Text reset
 
# Feedback indicators
info=${bldwht}*${txtrst}
pass=${bldblu}*${txtrst}
warn=${bldred}!${txtrst}
 
# Indicator usage
echo -e "${info} "
echo -e "${pass} "
echo -e "${warn} "
 
# Check if root
# if [ $(whoami) != root ]; then
#   echo " Must be root to use."
#   exit
# fi
 
# Check if selection exists
if [ ! -e "$@" ]; then
  echo " Selection \""$@"\" does not exist."
  exit
fi