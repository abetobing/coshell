#!/bin/sh
#
#
ENV=$1;
SERVER=$2;

if [ "$1" == "" ]; then
	ENV="default"
fi

echo 'Setting up ' $ENV ' environments'
if [ "$ENV" == "prod" ] || [ "$ENV" == "production" ]; then
	sudo cp /etc/envhosts/production /etc/hosts
elif [ "$ENV" == "staging" ]; then
	sudo cp /etc/envhosts/staging /etc/hosts
elif [ "$ENV" == "verify" ]; then
	sudo cp /etc/envhosts/verify /etc/hosts
elif [ "$ENV" == "local" ] && [ ! -z "$SERVER" ]; then
	sudo cp "/etc/envhosts/local_${SERVER}" /etc/hosts
else
	sudo cp /etc/envhosts/original /etc/hosts
fi
echo 'Done...'

echo "Do you want to open /etc/hosts (y/n)?"
read -t 10 -p "answer y/n or this app will exit in 10 seconds... " yn
case $yn in
	[Yy]* ) sudo vim /etc/hosts; break;;
	[Nn]* ) break;;
	* ) break;;
esac
