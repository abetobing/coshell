#!/bin/bash

# Text color variables
txtred='\033[0;31m'       # red
txtgrn='\033[0;32m'       # green
txtylw='\033[0;33m'       # yellow
txtblu='\033[0;34m'       # blue
txtpur='\033[0;35m'       # purple
txtcyn='\033[0;36m'       # cyan
txtwht='\033[0;37m'       # white
bldred='\033[1;31m'       # red    - Bold
bldgrn='\033[1;32m'       # green
bldylw='\033[1;33m'       # yellow
bldblu='\033[1;34m'       # blue
bldpur='\033[1;35m'       # purple
bldcyn='\033[1;36m'       # cyan
bldwht='\033[1;37m'       # white
txtund=$(tput sgr 0 1)  # Underline
txtbld=$(tput bold)     # Bold
txtrst='\033[0m'          # Text reset
 
# Feedback indicators
info=${bldwht}*${txtrst}
pass=${bldblu}*${txtrst}
warn=${bldred}!${txtrst}



CMD=`basename $0`

show_help()
{
    echo "Usage: $CMD <URL> [FORCE={0/1}]"
}

find_unique_filename()
{
    if [ $# -ne 2 ]; then
        echo "Fail! -- Expecting 2 arguments! ==> $@"
        return 1 # non-zero as false
    fi
    local UNIQUE_FILENAME_VAR=$1
    local FILENAME=$2
    if [ -f $FILENAME ]; then
        COUNTER=0
        while [ -f $FILENAME ]; do
            echo "File exists: `basename $FILENAME`"
            local NEW_FILENAME=`echo $FILENAME | sed "s#\(.*\)-[0-9]\+\.#\1-$COUNTER\.#g"`
            if [ "$NEW_FILENAME" == "$FILENAME" ]; then
                NEW_FILENAME=`echo $FILENAME | sed "s#\(.*\)\.#\1-$COUNTER\.#g"`
            fi
            FILENAME=$NEW_FILENAME
            COUNTER=$(( $COUNTER + 1 ))
        done
        echo "Next unique filename: `basename $FILENAME`"
    fi
    eval "$UNIQUE_FILENAME_VAR=$FILENAME"
}

cleanup()
{
    if [ $# -ne 3 ]; then
        echo "Fail! -- Expecting 3 argument! ==> $@"
        return 1 # non-zero as false
    fi
    local PLACEHOLDER_TEMP=$1
    local PARTIAL_TEMP=$2
    local DONE_VAR=$3
    local DONE_VALUE=1
    eval "DONE_VALUE=\$$DONE_VAR"
    if [ $DONE_VALUE -eq 0 ]; then
        if [ -f $PLACEHOLDER_TEMP ]; then
            echo  "Safely removing placeholder temp file ==> $PLACEHOLDER_TEMP"
            rm -f $PLACEHOLDER_TEMP
        fi
    fi
    if [ -f $PARTIAL_TEMP ]; then
        echo  "Safely removing partial temp file ==> $PARTIAL_TEMP"
        rm -f $PARTIAL_TEMP
    fi
}

if [ $# -ne 1 -a $# -ne 2 ]; then
    echo "Fail! -- Expecting 1 or 2 arguments! ==> $@"
    show_help
    exit 1
fi

if [ -z "`which curl`" ]; then
    echo "Fail! -- Requires \"curl\""
    echo "Hint: sudo aptitude install curl"
    exit 1
fi

if [ -z "`which wget`" ]; then
    echo "Fail! -- Requires \"wget\""
    echo "Hint: sudo aptitude install wget"
    exit 1
fi

if [ -f $1 ]; then
    if [ -f "xhdlf.sh" ]; then
        ./xhdlf.sh $1
        exit 1
    else
        echo "Fail! -- Requires \"xhdlf.sh\""
    fi
fi

URL=$1
FORCE=$2

if [ -z "$FORCE" ]; then
    FORCE=0
fi


echo ""
echo -e "${bldylw}Extracting video URL from.. ==> ${txtcyn}$URL${txtrst}"
EXTRACTED_HTML=`curl -s $URL` 
EXTRACTED_SRV=$(echo "$EXTRACTED_HTML" | grep -E "'(http:\/\/[a-z0-9]+\.xhcdn\.com)'" | sed 's#\ ##g' | sed "s#'srv':'##g" | sed "s#',##g" | sed "s#\r##g")
echo $EXTRACTED_SRV
# exit 1
if [[ -z "$EXTRACTED_SRV" ]]; then
    echo -e "${info} ${txtgrn}---------- MODE 001 ----------${txtrst}"
    EXTRACTED_PATH=$(echo "$EXTRACTED_HTML" | grep -E "\'file\':[\s]*(.+)\'," | sed 's#\ ##g' | sed "s#'file':'##g" | sed "s#',##g" | sed "s#\r##g")
    EXTRACTED_URL=`echo "$EXTRACTED_PATH"        | 
                    sed 's#\%3A#:#g'            | # replace %3A --> :
                    sed 's#\%3B#;#g'            | # replace %3B --> ;
                    sed 's#\%2F#/#g'            | # replace %2F --> /
                    sed 's#\%3F#?#g'            | # replace %3F --> ?
                    sed 's#\%2C#,#g'            | # replace %2C --> ,
                    sed 's#\%3D#=#g'            | # replace %3D --> =
                    sed 's#\%26#\&#g'`            # replace %26 --> &

    if [[ -z "$EXTRACTED_PATH" ]]; then

        echo -e "${info} ${txtgrn}---------- MODE 002 ----------${txtrst}"
        # http%3A%2F%2F0.xhcdn.com&file=2rEftnO8zoE%2Cend%3D1392758562%2Fdata%3DDE781BB0%2Fspeed%3D375k%2F2738830.flv
        EXTRACTED_SRV=$(echo "$EXTRACTED_HTML" | egrep -o "srv=http.+\.xhcdn\.com.+(mp4|flv)") 
        EXTRACTED_SRV=`echo "$EXTRACTED_SRV"        | 
                        sed 's#\%3A#:#g'            | # replace %3A --> :
                        sed 's#\%3B#;#g'            | # replace %3B --> ;
                        sed 's#\%2F#/#g'            | # replace %2F --> /
                        sed 's#\%3F#?#g'            | # replace %3F --> ?
                        sed 's#\%2C#,#g'            | # replace %2C --> ,
                        sed 's#\%3D#=#g'            | # replace %3D --> =
                        sed 's#\%26#\&#g'`            # replace %26 --> &
        EXTRACTED_PATH=$(echo "$EXTRACTED_SRV" | egrep -o "file=.+" | sed "s/file=//g")
        EXTRACTED_SRV=$(echo "$EXTRACTED_SRV" | egrep -o "http.+\.com")
        EXTRACTED_URL=$(echo "${EXTRACTED_SRV}/key=${EXTRACTED_PATH}" | tr -d '\r')

        if [[ -z "$EXTRACTED_PATH" ]]; then
            echo -e "${info} ${txtgrn}---------- MODE 003 ----------${txtrst}"
            # <video poster="http://ut30.xhcdn.com/t/730/320/6_2740730.jpg" controls type='video/mp4' file="http://0.xhcdn.com/key=vj-94mOV1h6,end=1392755485/data=ED448E7E/speed=/2740730.mp4">
            EXTRACTED_URL=$(echo "$EXTRACTED_HTML" | grep -E -w "<video\ .*file=\"(http:\/\/.*)+\">" | egrep -o "file=\"(http:\/\/.*)+\"" | egrep -o "http:\/\/[^\"]+") 
            echo "ONLY GOT $EXTRACTED_PATH"
            # exit 1
            if [[ -z $EXTRACTED_URL ]]; then
                echo -e "${info} ${txtgrn}---------- MODE 004 ----------${txtrst}"
                EXTRACTED_URL=$(echo "$EXTRACTED_HTML" | egrep -o "http:\/\/.+\.xhcdn\.com\/key=[^\"]+") 
                if [[ -z $EXTRACTED_URL ]]; then
                    echo -e "${warn} ${bldred}Video file not found. Terminated. ${txtrst}"
                    exit 1
                fi
            fi
        fi
    else
       EXTRACTED_URL=$(echo "${EXTRACTED_SRV}/key=${EXTRACTED_PATH}" | tr -d '\r')
    fi

else
    echo -e "${info} ${txtgrn}---------- MODE 005 ----------${txtrst}"
    EXTRACTED_PATH=$(echo "$EXTRACTED_HTML" | grep -E "\'file\':(.+)\.flv" | sed 's#\ ##g' | sed "s#'file':'##g" | sed "s#',##g" | sed "s#\r##g")
    EXTRACTED_PATH=`echo "$EXTRACTED_PATH"        | 
                    sed 's#\%3A#:#g'            | # replace %3A --> :
                    sed 's#\%3B#;#g'            | # replace %3B --> ;
                    sed 's#\%2F#/#g'            | # replace %2F --> /
                    sed 's#\%3F#?#g'            | # replace %3F --> ?
                    sed 's#\%2C#,#g'            | # replace %2C --> ,
                    sed 's#\%3D#=#g'            | # replace %3D --> =
                    sed 's#\%26#\&#g'`            # replace %26 --> &

    echo "GOT $EXTRACTED_PATH"
    EXTRACTED_URL=$(echo "${EXTRACTED_SRV}/key=${EXTRACTED_PATH}" | tr -d '\r')
fi
echo ""
echo -e "${txtylw}Downloading video from.. ==> ${bldcyn}$EXTRACTED_URL ${txtrst}"
# exit 1
FILE_EXT=`echo "$EXTRACTED_URL" | egrep -o "(\.flv|\.mp4)"`
LOCAL_FILE="`pwd`/`basename $URL`${FILE_EXT}"
LOCAL_FILE=`echo $LOCAL_FILE | sed "s/.html//g" |sed "s/ /_/g"`
if [ $FORCE -eq 1 ] && [ -f $LOCAL_FILE ]; then
    echo "Overwriting.. ==> `basename $LOCAL_FILE`"
else
    if find_unique_filename LOCAL_FILE $LOCAL_FILE; then
        : # do nothing
    else
        exit 1
    fi
    echo -e "${txtylw}Saving as new.. ==> ${bldcyn}`basename $LOCAL_FILE`${txtrst}"
fi
touch $LOCAL_FILE
TEMP=${LOCAL_FILE}.part
trap "cleanup $LOCAL_FILE $TEMP DONE" EXIT
DONE=0
# curl $EXTRACTED_URL > $TEMP
USER_AGENT="Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/534.7 (KHTML, like Gecko) Ubuntu/10.04 Chromium/7.0.517.44 Chrome/7.0.517.44 Safari/534.7" 
echo -e "${info} ${bldgrn}Summonning wget${txtrst}"
wget -bc --no-use-server-timestamps -U "'$USER_AGENT'" $EXTRACTED_URL -O $TEMP
mv $TEMP $LOCAL_FILE
DONE=1

echo ""
echo -e "${bldgrn}${info}Download complete! ==> ${bldcyn}$LOCAL_FILE ${txtrst}"
